import 'package:flutterreduxapp/models/model.dart';

class GalleryState   {
  final List<Item> items;

  GalleryState({
    this.items,
  });

  GalleryState.initialState() : items = List.unmodifiable(<Item>[]);

  GalleryState.fromJson(Map json)
      : items = (json['items'] as List).map((i) => Item.fromJson(i)).toList();

  Map toJson() => {'items': items};
}
