import 'package:flutterreduxapp/models/model.dart';

class CounterState {
  final List<Item> items;

  CounterState({
    this.items,
  });

  CounterState.initialState() : items = List.unmodifiable(<Item>[]);

  CounterState.fromJson(Map json)
      : items = (json['items'] as List).map((i) => Item.fromJson(i)).toList();

  Map toJson() => {'items': items};
}