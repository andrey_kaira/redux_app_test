import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutterreduxapp/states/counter_state.dart';
import 'package:flutterreduxapp/states/gallery_state.dart';
import 'package:redux/redux.dart';
import 'package:redux_dev_tools/redux_dev_tools.dart';

import 'models/model.dart';

void main() => runApp(Application());

class Application extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData.dark(),
      home: MyApp(),
    );
  }
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Test'),),
    );
  }
}


class _ViewModel {
  final List<Item> items;
  final Function(String) onAddItem;
  final Function(Item) onRemoveItem;
  final Function() onRemoveItems;

  _ViewModel({
    this.items,
    this.onAddItem,
    this.onRemoveItem,
    this.onRemoveItems,
  });

  factory _ViewModel.create(Store<CounterState> counterStore, Store<GalleryState> galleryStore) {
   // _onAddItem(String body) {
   //   store.dispatch(AddItemAction(body));
   // }
//
   // _onRemoveItem(Item item) {
   //   store.dispatch(RemoveItemAction(item));
   // }
//
   // _onRemoveItems() {
   //   store.dispatch(RemoveItemsAction());
   // }

    return _ViewModel(
      //items: store.state.items,
      //onAddItem: _onAddItem,
      //onRemoveItem: _onRemoveItem,
      //onRemoveItems: _onRemoveItems,
    );
  }
}







