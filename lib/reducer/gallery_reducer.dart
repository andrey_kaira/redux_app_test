import 'package:flutterreduxapp/states/gallery_state.dart';

import 'item_reducer.dart';

GalleryState galleryStateReducer(GalleryState state, action) {
  return GalleryState(
    items: itemReducer(
      state.items,
      action,
    ),
  );
}