import 'package:flutterreduxapp/states/counter_state.dart';

import 'item_reducer.dart';

CounterState counterStateReducer(CounterState state, action) {
  return CounterState(
    items: itemReducer(
      state.items,
      action,
    ),
  );
}
